#include <iostream>
#include <vector>

void FillObjects (
                     std::vector <int> & pvi_ObjectGroup,
                     const unsigned int  pi_Size
                 );

void CreateNextGen (std::vector <int> & pvi_ObjectGroup, const unsigned int li_GroupSize, unsigned int &pi_Idicator);

int main ()
{

    std::vector <int> lvi_ObjectGroup = {};
    unsigned int      li_Indicator    = 1;

    FillObjects(lvi_ObjectGroup, 10000);

    while (lvi_ObjectGroup.size() > 2)
        CreateNextGen(lvi_ObjectGroup, lvi_ObjectGroup.size(), li_Indicator);

    return 0;

}

void FillObjects (
                     std::vector <int> & pvi_ObjectGroup,
                     const unsigned int  pi_Size
                 )
{

    for (unsigned int i = 0; i < pi_Size; ++i)
        pvi_ObjectGroup.push_back(i + 1);

}

void CreateNextGen (
                       std::vector <int> & pvi_ObjectGroup,
                       const unsigned int  pi_GroupSize,
                       unsigned int      & pi_Idicator
                   )
{

    unsigned int li_Index      = 0;


    for (unsigned int i = 0; i < pi_GroupSize; i++) {

        if ((pi_Idicator %= 3) == 0)
            pvi_ObjectGroup.erase(pvi_ObjectGroup.begin() + li_Index);
        else
            ++li_Index;

        ++pi_Idicator;

    }

}
